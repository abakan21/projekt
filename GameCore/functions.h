#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
#include <algorithm>
#include <typeinfo>
#include <vector>
#include <random>
#include <climits>

#include "classwithid.h"



const std::string fillSpaces(const std::string &str);
const std::string makeSpaces(const std::string &str);
// from 0 to 255 (0% - 100%)
bool randomize(unsigned char chance);

// patterns -------------------------------------------------------8

// returns true if information was added successfully
template <typename T>
bool addInfo(std::vector<T> &container, T info)
{
    auto found = std::find_if(container.begin(), container.end(), [info](const T &t)->bool
    {
        return (t.name == info.name);
    });
    
    if (found == container.end())
    {
        container.push_back(info);
        return 0;
    }
    return 1;
}

// returns true if information was delete dsuccessfully
template <typename T>
bool deleteInfo(std::vector<T> &container, std::string name)
{
    auto found = std::find_if(container.begin(), container.end(), [name](const T &t)->bool
    {
        return (t.name == name);
    });
    
    if (found != container.end())
    {
        container.erase(found);
        return 0;
    }
    return 1;
}

// returns true if information was bind dsuccessfully
template <typename T>
bool bindInfo(std::vector<T> &container, typename std::vector<T>::iterator &iter, std::string name)
{
    typename std::vector<T>::iterator found = std::find_if(container.begin(), container.end(), [name](const T &t)->bool
    {
        return (t.name == name);
    });
    
    if (found != container.end())
    {
        iter = found;
        return 0;
    }
    return 1;
}

template <typename T>
bool iterIsValid(const T &iter)
{
    try
    {
        (*iter);
    }
    catch (...)
    {
        return false;
    }
    return true;
}

template <typename T>
bool ptrIsValid(const T *t)
{
    if (t == nullptr)
    {
        return false;
    }
    
    try
    {
        (*t);
    }
    catch (...)
    {
        return false;
    }
    return true;
}

template <typename T>
bool wasEvade(T t, int randNum)
{
    const unsigned char normRandNum = randNum % UCHAR_MAX;
    return (normRandNum > t.dexterity);
}

template<typename T>
typename std::vector<T *>::iterator
findByID(std::vector<T *> & container, ID_t id)
{
    return std::find_if(container.begin(), container.end(),
                                 [&id](const T *itemPtr)
    {
        return (itemPtr->getID() == id);
    });
}

template<typename T>
typename std::vector<T *>::const_iterator
findByID(const std::vector<T *> & container, ID_t id)
{
    return std::find_if(container.cbegin(), container.cend(),
                                 [&id](const T *itemPtr)
    {
        return (itemPtr->getID() == id);
    });
}

#endif // FUNCTIONS_H
