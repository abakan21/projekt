#include "functions.h"

const std::string fillSpaces(const std::string &str)
{
    std::string result(str);
    
    for (char &i : result)
    {
        if (i == ' ')
        {
            i = '$';
        }
    }
    
    return result;
}

const std::string makeSpaces(const std::string &str)
{
    std::string result(str);
    
    for (char &i : result)
    {
        if (i == '$')
        {
            i = ' ';
        }
    }
    
    return result;
}

bool randomize(unsigned char chance)
{
    // 0..254 < 0 and 0..254 < 255
    return rand() % 255 < chance;
}
