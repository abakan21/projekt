#include "functionsWithHero.h"

void printSimpleHero(std::ostream &os, SimpleHero &bh)
{
    os << "Name: " << bh.getGameObjectInfo().name << "\n"
       << "Description: " << bh.getGameObjectInfo().description << "\n"
       << "MaxHealth: " << bh.getBasicHeroInfo().maxHealth << "\n"
       << "MaxCapacity: " << bh.getBasicHeroInfo().maxCapacity << "\n"
       << "UnarmedDamage: " << bh.getBasicHeroInfo().unarmedDamage << "\n"
       << "Dexterity: " << SpecialFraction::makeFloatFromUChar(
              bh.getBasicHeroInfo().dexterity) << "\n"
       << "LevelMultipler: " << SpecialFraction::makeFloatFromUChar(
              bh.getBasicHeroInfo().levelMultipler) << "\n";
}

BasicObject * loadSimpleObject(std::ifstream &ifs)
{
    BasicObject *result = nullptr;
    
    std::string name;
    ifs >> name;
    if (name == typeid(SimpleObject).name())
    {
        result = new SimpleObject;
        dynamic_cast<SimpleObject *>(result)->load(ifs);
    }
    
    return result;
}

void saveSimpleObject(std::ofstream &ofs, const BasicObject *bo)
{
    ofs << bo->getClassName() << "\n";
    
    bo->save(ofs);
}

BasicMonster * loadSimpleMonster(std::ifstream &ifs)
{
    BasicMonster *result = nullptr;
    
    std::string name;
    ifs >> name;
    if (name == typeid(SimpleMonster).name())
    {
        result = new SimpleMonster;
        dynamic_cast<SimpleMonster *>(result)->load(ifs);
    }
    
    return result;
}

void saveSimpleMonster(std::ofstream &ofs, const BasicMonster *bm)
{
    ofs << bm->getClassName() << "\n";
    
    bm->save(ofs);
}
