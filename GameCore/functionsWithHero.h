#ifndef FUNCTIONSWITHHERO_H
#define FUNCTIONSWITHHERO_H

#include "functionsWithItems.h"

#include "heroes/simplehero.h"
#include "specialfraction.h"
#include "monsters/simplemonster.h"
#include "objects/simpleobject.h"



void printSimpleHero(std::ostream &os, SimpleHero &bh);

BasicObject * loadSimpleObject(std::ifstream &ifs);
void saveSimpleObject(std::ofstream &ofs, const BasicObject *bo);
BasicMonster * loadSimpleMonster(std::ifstream &ifs);
void saveSimpleMonster(std::ofstream &ofs, const BasicMonster *bm);



#endif // FUNCTIONSWITHHERO_H
