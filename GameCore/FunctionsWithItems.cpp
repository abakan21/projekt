#include "functionsWithItems.h"

void printItemFood(std::ostream &os, ItemFood &iF)
{
    os << "Name: " << iF.getGameObjectInfo().name << "\n"
       << "Description: " << iF.getGameObjectInfo().description << "\n"
       << "Weight: " << iF.getBasicItemInfo().weight << "\n"
       << "Curative: " << iF.getItemFoodInfo().curative << "\n";
}
void printItemWeapon(std::ostream &os, ItemWeapon &iw)
{
    os << "Name: " << iw.getGameObjectInfo().name << "\n"
       << "Description: " << iw.getGameObjectInfo().name << "\n"
       << "Weight: " << iw.getBasicItemInfo().weight << "\n"
       << "Damage: " << iw.getItemWeaponInfo().damage << "\n"
       << "MaxDurability: " << iw.getItemWeaponInfo().maxDurability << "\n"
       << "Durability: " << iw.getItemWeaponInfo().durability << "\n";
}

BasicItem * loadItem(std::ifstream &ifs)
{
    BasicItem *result = nullptr;
    
    std::string name;
    ifs >> name;
    if (name == typeid(ItemFood).name())
    {
        result = new ItemFood;
        dynamic_cast<ItemFood *>(result)->load(ifs);
    }
    else if (name == typeid(ItemWeapon).name())
    {
        result = new ItemWeapon;
        dynamic_cast<ItemWeapon *>(result)->load(ifs);
    }
    
    return result;
}

void saveItem(std::ofstream &ofs, const BasicItem *bi)
{
    ofs << bi->getClassName() << "\n";
    
    bi->save(ofs);
}
