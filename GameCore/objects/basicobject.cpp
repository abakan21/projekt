#include "basicobject.h"

BasicObject::BasicObject(std::string name, std::string description,
                           unsigned int levelAdd)
    : GameObject(name, description),
      m_basicObjectInfo{levelAdd, false}
{
    
}

BasicObject::~BasicObject()
{
    deleteLoot();
}

void BasicObject::addLootItem(BasicItem *item, unsigned char chance)
{
    m_loot.push_back(std::make_pair(item, chance));
}

bool BasicObject::removeLootItem(ID_t itemID)
{
    for (auto i = m_loot.begin(); i != m_loot.end(); ++i)
    {
        if ((*i).first->getID() == itemID)
        {
            delete (*i).first;
            m_loot.erase(i);
            return true;
        }
    }
    return false;
}

const BasicObject::LootContainer_t & BasicObject::getLootItems() const
{
    return m_loot;
}

const BasicObjectInfo & BasicObject::getBasicObjectInfo() const
{
    return m_basicObjectInfo;
}

void BasicObject::setBasicObjectInfo(BasicObjectInfo info)
{
    m_basicObjectInfo = info;
}

void BasicObject::load(std::ifstream &ifs)
{
    GameObject::load(ifs);
    ifs >> m_basicObjectInfo.levelAdd
            >> m_basicObjectInfo.wasExamined;
    
    unsigned int size;
    ifs >> size;
    deleteLoot();
    std::string name;
    unsigned char chance;
    for (unsigned int i = 0; i < size; ++i)
    {
        BasicItem *loadedItem = loadItem(ifs);
        
        if (loadedItem == nullptr)
        {
            return;
        }
        ifs >> chance;
        m_loot.push_back(std::make_pair(loadedItem, chance));
    }
}

void BasicObject::save(std::ofstream &ofs) const
{
    GameObject::save(ofs);
    ofs << m_basicObjectInfo.levelAdd << " "
        << m_basicObjectInfo.wasExamined << "\n";
    
    ofs << m_loot.size() << "\n";
    for (auto i = m_loot.begin(); i != m_loot.end(); ++i)
    {
        (*i).first->save(ofs);
        
        ofs << (*i).second << "\n";
    }
}

void BasicObject::resetExamined()
{
    m_basicObjectInfo.wasExamined = false;
}

void BasicObject::examine(BasicObject::ContainerItems_t &items)
{
    items.clear();
    
    if (!m_basicObjectInfo.wasExamined)
    {
        m_basicObjectInfo.wasExamined = true;
        
        for (auto i : m_loot)
        {
            if (randomize(i.second))
            {
                items.push_back(i.first->clone());
            }
        }
    }
}

// private ---------------------------------------------

void BasicObject::deleteLoot()
{
    for (auto i : m_loot)
    {
        delete i.first;
    }
    m_loot.clear();
}
