#include "roomsimple.h"

RoomSimple::RoomSimple(std::string name, std::string description)
    : BasicRoom(name, description)
{
    
}

void RoomSimple::addMonster(BasicMonster *monster)
{
    m_monsters.push_back(monster);
}

void RoomSimple::addObject(BasicObject *object)
{
    m_objects.push_back(object);
}

const RoomSimple::MonsterContainer_t & RoomSimple::getMonsters() const
{
    return m_monsters;
}

const RoomSimple::ObjectContainer_t & RoomSimple::getObjects() const
{
    return m_objects;
}

void RoomSimple::load(std::ifstream &ifs)
{
    unsigned int size;
    
    ifs >> size;
    m_monsters.clear();
    for (unsigned int i = 0; i < size; ++i)
    {
        m_monsters.push_back(loadSimpleMonster(ifs));
    }
    
    ifs >> size;
    m_objects.clear();
    for (unsigned int i = 0; i < size; ++i)
    {
        m_objects.push_back(loadSimpleObject(ifs));
    }
}

void RoomSimple::save(std::ofstream &ofs) const
{
    ofs << m_monsters.size() << "\n";
    for (auto i = m_monsters.begin(); i != m_monsters.end(); ++i)
    {
        saveSimpleMonster(ofs, *i);
    }
    
    ofs << m_objects.size() << "\n";
    for (auto i = m_objects.begin(); i != m_objects.end(); ++i)
    {
        saveSimpleObject(ofs, *i);
    }
}

BasicRoom * RoomSimple::clone() const
{
    return new RoomSimple(*this);
}
