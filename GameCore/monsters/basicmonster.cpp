//
// Created by Asus on 15.04.2021.
//

#include <iostream>
#include "basicmonster.h"

BasicMonster::BasicMonster(std::string name, std::string description,
                           unsigned int maxHealth,
                           unsigned int damage, unsigned int exp,
                           SpecialFraction dexterity)
    : GameObject(name, description),
      m_basicMonsterInfo{maxHealth, maxHealth, damage, exp, dexterity.getChar()}
{
    
}

const BasicMonsterInfo & BasicMonster::getBasicMonsterInfo() const
{
    return m_basicMonsterInfo;
}

void BasicMonster::setBasicMonsterInfo(BasicMonsterInfo info)
{
    m_basicMonsterInfo = info;
}

//void BasicMonster::increaseDamage(unsigned int damage) {
//    if (m_health <  0.5 * m_health ){
//        m_damage = (m_damage *1.5);
//    }
//}

void BasicMonster::setFullHealth()
{
    m_basicMonsterInfo.health = m_basicMonsterInfo.maxHealth;
}

bool BasicMonster::hit(unsigned int damage)
{
    if (m_basicMonsterInfo.health > 0)
    {
        if (m_basicMonsterInfo.health > damage)
        {
            m_basicMonsterInfo.health -= damage;
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

void BasicMonster::load(std::ifstream &ifs)
{
    GameObject::load(ifs);
    ifs >> m_basicMonsterInfo.maxHealth
            >> m_basicMonsterInfo.health
            >> m_basicMonsterInfo.damage
            >> m_basicMonsterInfo.exp
            >> m_basicMonsterInfo.dexterity;
}

void BasicMonster::save(std::ofstream &ofs) const
{
    GameObject::save(ofs);
    ofs << m_basicMonsterInfo.maxHealth << " "
        << m_basicMonsterInfo.health << " "
        << m_basicMonsterInfo.damage << " "
        << m_basicMonsterInfo.exp << " "
        << m_basicMonsterInfo.dexterity << "\n";
}
