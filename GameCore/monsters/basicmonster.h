//
// Created by Asus on 15.04.2021.
//

#ifndef TEST_PROJEKT_BASICMONSTER_H
#define TEST_PROJEKT_BASICMONSTER_H

#include "../gameobject.h"
#include "../functions.h"
#include "../specialfraction.h"



struct BasicMonsterInfo
{
    unsigned int    maxHealth;
    unsigned int    health;
    unsigned int    damage;
    unsigned int    exp;
    unsigned char   dexterity;
};

class BasicMonster : public GameObject
{

public:
    BasicMonster() {};
    BasicMonster(std::string name, std::string description,
                 unsigned int maxHealth, unsigned int damage,
                 unsigned int exp, SpecialFraction dexterity);
    virtual ~BasicMonster() {};
    
    const BasicMonsterInfo & getBasicMonsterInfo() const;
    void setBasicMonsterInfo(BasicMonsterInfo info);
    
//    void increaseDamage(unsigned int damage);
    
    void setFullHealth();
    bool hit(unsigned int damage);
    
    virtual void load(std::ifstream &ifs) override;
    virtual void save(std::ofstream &ofs) const override;
    
    virtual BasicMonster *clone() const = 0;
    
protected:
    BasicMonsterInfo m_basicMonsterInfo;
};

#endif //TEST_PROJEKT_BASICMONSTER_H
