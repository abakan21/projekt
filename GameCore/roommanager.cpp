#include "roommanager.h"

RoomManager::~RoomManager()
{
    deleteAllRooms();
}

bool RoomManager::connectRoom(ID_t first, ID_t second)
{
    auto firstRoomIter = findByID(m_rooms, first);
    auto secondRoomIter = findByID(m_rooms, second);
    
    if (firstRoomIter != m_rooms.end()
            && secondRoomIter != m_rooms.end())
    {
        (*firstRoomIter)->addConnection(*secondRoomIter);
        (*secondRoomIter)->addConnection(*firstRoomIter);
        return true;
    }
    
    return false;
}

bool RoomManager::connectLastRoom(ID_t first)
{
    return connectRoom(first, m_rooms.size() - 1);
}

void RoomManager::addRoom(BasicRoom *roomp)
{
    m_rooms.push_back(roomp);
}

bool RoomManager::removeRoom(ID_t roomID)
{
    for (auto i = m_rooms.begin(); i != m_rooms.end(); ++i)
    {
        if ((*i)->getID() == roomID)
        {
            (*i)->removeAllConnectionsFromAllRooms();
            
            delete *i;
            m_rooms.erase(i);
            return true;
        }
    }
    return false;
}

const RoomManager::RoomContainer_t & RoomManager::getRooms() const
{
    return m_rooms;
}

// private -------------------------------------------------------------

void RoomManager::deleteAllRooms()
{
    for (auto i : m_rooms)
    {
        delete i;
    }
    m_rooms.clear();
}
