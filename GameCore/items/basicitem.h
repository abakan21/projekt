#ifndef BASIC_ITEM_H
#define BASIC_ITEM_H

#include "../gameobject.h"



struct BasicItemInfo
{
    unsigned int weight;
};

class BasicItem : public GameObject
{
    
public:
    BasicItem() {};
    BasicItem(std::string name, std::string description, unsigned int weight);
    virtual ~BasicItem() {};
    
    const BasicItemInfo & getBasicItemInfo() const;
    void setBasicItemInfo(BasicItemInfo info);
    
    virtual void load(std::ifstream &ifs) override;
    virtual void save(std::ofstream &ofs) const override;
    
    virtual BasicItem *clone() const = 0;
    
protected:
    BasicItemInfo m_basicItemInfo;
};

#endif // BASIC_ITEM_H
