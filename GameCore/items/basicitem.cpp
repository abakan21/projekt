#include "basicitem.h"

BasicItem::BasicItem(std::string name, std::string description,
                       unsigned int weight)
    : GameObject(name, description),
      m_basicItemInfo{weight}
{
    
}

const BasicItemInfo & BasicItem::getBasicItemInfo() const
{
    return m_basicItemInfo;
}

void BasicItem::setBasicItemInfo(BasicItemInfo info)
{
    m_basicItemInfo = info;
}

void BasicItem::load(std::ifstream &ifs)
{
    GameObject::load(ifs);
    ifs >> m_basicItemInfo.weight;
}

void BasicItem::save(std::ofstream &ofs) const
{
    GameObject::save(ofs);
    ofs << m_basicItemInfo.weight << "\n";
}
