#include "itemweapon.h"

ItemWeapon::ItemWeapon(std::string name, std::string description,
                         unsigned int weight, unsigned int damage,
                         unsigned int maxDurability, unsigned int durability)
    : BasicItem(name, description, weight),
      m_itemWeaponInfo{damage, maxDurability, durability}
{
    
}

const ItemWeaponInfo & ItemWeapon::getItemWeaponInfo() const
{
    return m_itemWeaponInfo;
}

void ItemWeapon::setItemWeaponInfo(ItemWeaponInfo info)
{
    m_itemWeaponInfo = info;
}

void ItemWeapon::setFullRepair()
{
    m_itemWeaponInfo.durability = m_itemWeaponInfo.maxDurability;
}

bool ItemWeapon::use()
{
    if (m_itemWeaponInfo.durability > 0)
    {
        m_itemWeaponInfo.durability--;
    }
    
    return !(m_itemWeaponInfo.durability > 0);
}

void ItemWeapon::randomizeDurability()
{
    m_itemWeaponInfo.durability = (rand() * rand() * (rand() % 4))
            % m_itemWeaponInfo.maxDurability + 1;
}

void ItemWeapon::load(std::ifstream &ifs)
{
    BasicItem::load(ifs);
    ifs >> m_itemWeaponInfo.damage
            >> m_itemWeaponInfo.maxDurability
            >> m_itemWeaponInfo.durability;
}

void ItemWeapon::save(std::ofstream &ofs) const
{
    BasicItem::save(ofs);
    ofs << m_itemWeaponInfo.damage << " "
        << m_itemWeaponInfo.maxDurability << " "
        << m_itemWeaponInfo.durability << "\n";
}

BasicItem *ItemWeapon::clone() const
{
    return new ItemWeapon(*this);
}
