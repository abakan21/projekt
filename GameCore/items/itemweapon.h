#ifndef ITEM_WEAPON_H
#define ITEM_WEAPON_H

#include "basicitem.h"



struct ItemWeaponInfo
{
    unsigned int    damage;
    unsigned int    maxDurability;
    unsigned int    durability;
};

class ItemWeapon : public BasicItem
{
public:
    ItemWeapon() {};
    ItemWeapon(std::string name, std::string description, unsigned int weight,
                unsigned int damage, unsigned int maxDurability,
               unsigned int durability);
    virtual ~ItemWeapon() {};
    
    const ItemWeaponInfo & getItemWeaponInfo() const;
    void setItemWeaponInfo(ItemWeaponInfo info);
    
    void setFullRepair();
    // returns true if item is broken
    bool use();
    void randomizeDurability();
    
    virtual void load(std::ifstream &ifs) override;
    virtual void save(std::ofstream &ofs) const override;
    
    virtual BasicItem *clone() const override;
    
    virtual const std::string getClassName() const override
    {return typeid(ItemWeapon).name();};
    
protected:
    ItemWeaponInfo m_itemWeaponInfo;
};

#endif // ITEM_WEAPON_H
