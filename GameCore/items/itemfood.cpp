#include "itemfood.h"

ItemFood::ItemFood(std::string name, std::string description,
                     unsigned int weight, unsigned int curative)
    : BasicItem(name, description, weight),
      m_itemFoodInfo{curative}
{
    
}

const ItemFoodInfo & ItemFood::getItemFoodInfo() const
{
    return m_itemFoodInfo;
}

void ItemFood::setItemFoodInfo(ItemFoodInfo info)
{
    m_itemFoodInfo = info;
}

void ItemFood::load(std::ifstream &ifs)
{
    BasicItem::load(ifs);
    ifs >> m_itemFoodInfo.curative;
}

void ItemFood::save(std::ofstream &ofs) const
{
    BasicItem::save(ofs);
    ofs << m_itemFoodInfo.curative << "\n";
}

BasicItem *ItemFood::clone() const
{
    return new ItemFood(*this);
}
