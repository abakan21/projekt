#include "gameobject.h"

GameObject::GameObject(std::string name, std::string description)
    : m_gameObjectInfo{name, description}
{
    
}

const GameObjectInfo & GameObject::getGameObjectInfo() const
{
    return m_gameObjectInfo;
}

void GameObject::setGameObjectInfo(GameObjectInfo gameObjectInfo)
{
    m_gameObjectInfo = gameObjectInfo;
}

void GameObject::load(std::ifstream &ifs)
{
    ifs >> m_gameObjectInfo.name >> m_gameObjectInfo.description;
    
    m_gameObjectInfo.name = makeSpaces(m_gameObjectInfo.name);
    m_gameObjectInfo.description = makeSpaces(m_gameObjectInfo.description);
}

void GameObject::save(std::ofstream &ofs) const
{
    ofs << fillSpaces(m_gameObjectInfo.name) << " "
        << fillSpaces(m_gameObjectInfo.description) << "\n";
}
